package luwakdev.bestonestore.Auth;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by Krisnasw on 10/24/2016.
 */

public class SQLiteHandler extends SQLiteOpenHelper {
    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 5;

    // Database Name
    private static final String DATABASE_NAME = "bestone_api";

    // Login table name
    private static final String TABLE_USER = "user";

    private static final String TABLE_CART = "cart";

    // Login Table Columns names
    private static final String KEY_ID = "id_user";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ALAMAT = "alamat";
    private static final String KEY_TELP = "telp";
    private static final String KEY_ROLE = "role";
    private static final String KEY_PROVINSI = "provinsi";
    private static final String KEY_KOTA = "kota";
    private static final String KEY_ZIPCODE = "zipcode";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
//        String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
//                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
//                + KEY_EMAIL + " TEXT UNIQUE," + KEY_ALAMAT + " TEXT,"
//                + KEY_TELP + " TEXT UNIQUE," + KEY_ROLE + " TEXT,"
//                + KEY_PROVINSI + " TEXT," + KEY_KOTA + " TEXT," + KEY_ZIPCODE + " TEXT" +")";

        db.execSQL("CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY, id_user VARCHAR, name VARCHAR, email VARCHAR, alamat VARCHAR," +
                "telp VARCHAR, role VARCHAR, kota VARCHAR, zipcode VARCHAR, provinsi VARCHAR);");

        /*db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_CART + "(id INTEGER PRIMARY KEY, id_user " +
                "VARCHAR, id_produk VARCHAR, qty_barang INTEGER, harga_asli INTEGER, sub INTEGER, " +
                "create_at DEFAULT CURRENT_TIMESTAMP)");*/

        Log.d(TAG, "Database tables created");
        System.out.println("GGWP Bous"+TAG);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        /*db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);*/

        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     * */
    public void addUser(String id_user, String name, String email, String alamat, String telp,
                        String provinsi, String kota, String zipcode) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, id_user);
        values.put(KEY_NAME, name); // Name
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_ALAMAT, alamat); // Alamat
        values.put(KEY_TELP, telp); // Telp
        values.put(KEY_PROVINSI, provinsi); // Provinsi
        values.put(KEY_KOTA, kota); // Kota
        values.put(KEY_ZIPCODE, zipcode); // Zipcode

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public void addCart(String name, String email, Integer alamat, Integer telp,
                        Integer provinsi) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, name);
        values.put(KEY_EMAIL, email);
        values.put(KEY_ALAMAT, alamat);
        values.put(KEY_TELP, telp);
        values.put(KEY_PROVINSI, provinsi);

        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    /**
     * Getting user data from database
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM user";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("id_user", cursor.getString(1));
            user.put("name", cursor.getString(2));
            user.put("email", cursor.getString(3));
            user.put("alamat", cursor.getString(4));
            user.put("telp", cursor.getString(5));
            user.put("provinsi", cursor.getString(6));
            user.put("kota", cursor.getString(7));
            user.put("zipcode", cursor.getString(8));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }

    /**
     * Re crate database Delete all tables and create them again
     * */
    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

    public void onUpgrade(String name, String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        // Create tables again
        onCreate(db);
    }
}