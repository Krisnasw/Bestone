package luwakdev.bestonestore.Intro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import luwakdev.bestonestore.Activity.HomeActivity;
import luwakdev.bestonestore.R;

public class Splash extends AppCompatActivity {

    private Intent intent;
    private static int splash_interval = 2000;
    private boolean status = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onPause() {
        super.onPause();
        status = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        status = true;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (status==true) {
                    intent = new Intent(Splash.this, HomeActivity.class);

                    startActivity(intent);
                    finish();
                }
            }
        }, splash_interval);
    }
}
