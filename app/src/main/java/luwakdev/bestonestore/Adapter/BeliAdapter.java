package luwakdev.bestonestore.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;
import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.Cart;
import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Activity.CartActivity;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.Other.ImageTransform;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by decrypt21 on 11/6/16.
 */

public class BeliAdapter extends BaseAdapter {

    Context context;
    MaterialNumberPicker numberPicker;

    ArrayList<Cart> cart;

    String id_user, id_produk, harga_asli;

    SQLiteHandler db;
    HashMap<String, String> user;

    Typeface fonts1,fonts2;

    public BeliAdapter(Context context, ArrayList<Cart> cart) {
        this.context = context;
        this.cart = cart;
    }

    @Override
    public int getCount() {
        return cart.size();
    }

    @Override
    public Object getItem(int position) {
        return cart.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        fonts1 =  Typeface.createFromAsset(context.getAssets(),
                "fonts/MavenPro-Regular.ttf");

        db = new SQLiteHandler(context);
        user = db.getUserDetails();
        id_user = user.get("id_user");

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_beli, null);

            viewHolder = new ViewHolder();

            viewHolder.progress_dialog = (ProgressBar) convertView.findViewById(R.id.progress_dialog);
            viewHolder.gambar_produk = (ImageView) convertView.findViewById(R.id.gambar_produk);
            viewHolder.nama_produk = (TextView) convertView.findViewById(R.id.nama_produk);
            viewHolder.harga_produk = (TextView) convertView.findViewById(R.id.harga_produk);
            viewHolder.qty_produk = (TextView) convertView.findViewById(R.id.qty_produk);

            viewHolder.nama_produk.setTypeface(fonts1);
            viewHolder.harga_produk.setTypeface(fonts1);
            viewHolder.qty_produk.setTypeface(fonts1);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Cart cart = (Cart) getItem(position);
        final ViewHolder finalViewHolder = viewHolder;

        id_produk = cart.getId_produk();
        harga_asli = cart.getHarga_asli();

        Glide.with(context).load(AppConfig.IMAGE_URL + cart.getImage())
                .placeholder(R.drawable.def_produk)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new ImageTransform(context))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        finalViewHolder.progress_dialog.setVisibility(View.GONE);
                        finalViewHolder.gambar_produk.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        finalViewHolder.progress_dialog.setVisibility(View.GONE);
                        finalViewHolder.gambar_produk.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(viewHolder.gambar_produk);

        viewHolder.nama_produk.setText(cart.getName_produk());

        DecimalFormat format = new DecimalFormat("#,###");
        String total = format.format(Integer.parseInt(cart.getHarga_asli()));

        viewHolder.harga_produk.setText("Rp. " + total + " ,-");
        viewHolder.qty_produk.setText(cart.getQty_barang());

        numberPicker = new MaterialNumberPicker.Builder(context)
                .minValue(1)
                .maxValue(10)
                .defaultValue(Integer.parseInt(cart.getQty_barang()))
                .backgroundColor(Color.WHITE)
                .separatorColor(Color.TRANSPARENT)
                .textColor(Color.BLACK)
                .textSize(20)
                .enableFocusability(false)
                .wrapSelectorWheel(true)
                .build();

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                numberPicker.setValue(newVal);
            }
        });

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Jumlah Barang")
                .setView(numberPicker)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.out.println("Id Produk : " + cart.getId_produk() + " Update Jumlah : " + numberPicker.getValue() + " Harga Asli : " + Integer.parseInt(cart.getHarga_asli()));
                        updateQty(cart.getId_produk(), numberPicker.getValue(), Integer.parseInt(cart.getHarga_asli()));
                    }
                })
                .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*System.out.println("Jumlah Barang : " + numberPicker.getValue() + " Id Produk : " + cart.getId_produk() + " Harga Asli : " + Integer.valueOf(cart.getHarga_asli()));
                */
                System.out.println("Number Picker : " + numberPicker.getValue());
                dialog.show();
            }
        });

        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Peringatan !")
                        .setContentText("Apakah Anda ingin Menghapus Item ini Dari Keranjang ?")
                        .setConfirmText("Ya")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                hapusItem(
                                    id_user, cart.getId_produk()
                                );

                                sDialog.dismissWithAnimation();
                            }
                        })
                        .setCancelText("Tidak")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();

                return true;
            }
        });

        return convertView;
    }

    public void updateQty(String id_produk, Integer qty, Integer harga_asli)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.updateQty(
                id_produk,
                id_user,
                qty,
                harga_asli
        );
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();

                if (json.getStatus() == 1) {
                    final SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setTitleText("Berhasil");
                    dialog.setContentText("Berhasil Mengubah Jumlah Barang");
                    dialog.setConfirmText("Ok");
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ((Activity) context).startActivity(new Intent(context, CartActivity.class));
                                ((Activity) context).finish();
                            } catch (Exception e) {
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismissWithAnimation();
                        }
                    }, 1000);
                }
                else {
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Gagal !")
                            .setContentText("Gagal Mengubah Jumlah Barang")
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }

    public void hapusItem(String param1, String param2)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.hapusItem(
                param1,
                param2
        );
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();

                if (json.getStatus() == 1) {
                    final SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setTitleText("Berhasil");
                    dialog.setContentText(json.getPesan());
                    dialog.setConfirmText("Ok");
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ((Activity) context).startActivity(new Intent(context, CartActivity.class));
                                ((Activity) context).finish();
                            } catch (Exception e) {
                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismissWithAnimation();
                        }
                    }, 1000);
                }
                else {
                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Gagal !")
                            .setContentText(json.getPesan())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }

    private class ViewHolder {
        ProgressBar progress_dialog;
        ImageView gambar_produk;
        TextView nama_produk, harga_produk, qty_produk;
    }
}