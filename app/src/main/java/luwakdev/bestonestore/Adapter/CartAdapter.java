package luwakdev.bestonestore.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import luwakdev.bestonestore.Cart.ItemCart1;
import luwakdev.bestonestore.Cart.ItemCart2;

/**
 * Created by decrypt21 on 11/5/16.
 */

public class CartAdapter extends FragmentStatePagerAdapter {

    int tabCount;

    public CartAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ItemCart1 tab1 = new ItemCart1();
                return tab1;
            case 1:
                ItemCart2 tab2 = new ItemCart2();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
