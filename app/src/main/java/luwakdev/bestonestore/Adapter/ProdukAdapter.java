package luwakdev.bestonestore.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.text.DecimalFormat;
import java.util.ArrayList;

import luwakdev.bestonestore.API.Produk;
import luwakdev.bestonestore.Activity.DetailActivity;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.Other.ImageTransform;
import luwakdev.bestonestore.R;

/**
 * Created by decrypt21 on 10/28/16.
 */

public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.ViewHolder> {

    ArrayList<Produk> produk;
    Context context;
    View view;

    Integer harga_barang;

    public ProdukAdapter(Context context, ArrayList<Produk> produk) {
        this.context = context;
        this.produk = produk;
    }

    @Override
    public ProdukAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_produk, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Glide.with(context).load(AppConfig.IMAGE_URL + produk.get(position).getImage())
                .placeholder(R.drawable.def_produk)
                .error(R.drawable.def_produk)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new ImageTransform(context))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        holder.gambar_produk.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progress.setVisibility(View.GONE);
                        holder.gambar_produk.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(holder.gambar_produk);

        DecimalFormat format = new DecimalFormat("#,###");
        harga_barang = Integer.parseInt(produk.get(position).getHarga());

        holder.status_produk.setText(produk.get(position).getStatus().toUpperCase());
        holder.nama_produk.setText(produk.get(position).getName_produk());
        holder.harga_produk.setText("Rp. " + format.format(harga_barang) + " ,-");
    }

    @Override
    public int getItemCount() {
        return produk.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView gambar_produk;
        private TextView status_produk, nama_produk, harga_produk;
        private ProgressBar progress;

        public ViewHolder(final View view) {
            super(view);

            progress = (ProgressBar) view.findViewById(R.id.progress);
            gambar_produk = (ImageView) view.findViewById(R.id.gambar_produk);

            status_produk = (TextView) view.findViewById(R.id.status_produk);
            nama_produk = (TextView) view.findViewById(R.id.nama_produk);
            harga_produk = (TextView) view.findViewById(R.id.harga_produk);

            /* SetOnClickListener di Dalam Recyclerview Adapter */
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(view.getContext(), DetailActivity.class);
                    intent.putExtra("id", produk.get(getAdapterPosition()).getId());
                    System.out.println("Posisi Item : " + getAdapterPosition());
                    view.getContext().startActivity(intent);
                }
            });

            int dimen = (int) context.getResources().getDimension(R.dimen.foto_search);

            progress.getLayoutParams().width = dimen;
            progress.getLayoutParams().height = dimen;

            gambar_produk.getLayoutParams().width = dimen;
            gambar_produk.getLayoutParams().height = dimen;
        }
    }
}
