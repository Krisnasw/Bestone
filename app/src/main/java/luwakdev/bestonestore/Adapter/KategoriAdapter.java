package luwakdev.bestonestore.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import luwakdev.bestonestore.API.Kategori;
import luwakdev.bestonestore.Activity.ProdukActivity;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.Other.ImageTransform;
import luwakdev.bestonestore.R;


public class KategoriAdapter extends BaseAdapter {

    Context context;

    ArrayList<Kategori> kategori;
    Typeface fonts1, fonts2;

    public KategoriAdapter(Context context, ArrayList<Kategori> produk) {
        this.context = context;
        this.kategori = produk;
    }

    @Override
    public int getCount() {
        return kategori.size();
    }

    @Override
    public Object getItem(int position) {
        return kategori.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        fonts1 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Light.ttf");

        fonts2 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_beranda, null);

            viewHolder = new ViewHolder();

            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.nama_kategori = (TextView) convertView.findViewById(R.id.nama_kategori);

            viewHolder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress);

            int dimen = (int) this.context.getResources().getDimension(R.dimen.foto_produk);
            viewHolder.image.getLayoutParams().width = dimen;
            viewHolder.image.getLayoutParams().height = dimen;

            viewHolder.nama_kategori.setTypeface(fonts2);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Kategori kategori = (Kategori) getItem(position);
        final ViewHolder finalViewHolder = viewHolder;

        Glide.with(context).load(AppConfig.IMAGE_URL + kategori.getImage())
                .placeholder(R.drawable.def_produk)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new ImageTransform(context))
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        finalViewHolder.progressBar.setVisibility(View.GONE);
                        finalViewHolder.image.setVisibility(View.VISIBLE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        finalViewHolder.progressBar.setVisibility(View.GONE);
                        finalViewHolder.image.setVisibility(View.VISIBLE);
                        return false;
                    }
                })
                .into(viewHolder.image);

        viewHolder.nama_kategori.setText(kategori.getNama_kategori());

        final View finalConvertView = convertView;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(finalConvertView.getContext(), ProdukActivity.class);
                intent.putExtra("id", kategori.getId());
                finalConvertView.getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView image;
        TextView nama_kategori;

        ProgressBar progressBar;
    }
}
