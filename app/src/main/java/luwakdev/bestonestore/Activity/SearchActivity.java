package luwakdev.bestonestore.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Arrays;

import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.Produk;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Adapter.ProdukAdapter;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.Other.MaterialSearch;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    MaterialSearch searchView;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ArrayList<Produk> list_produk;
    private ProdukAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        recyclerView = (RecyclerView) findViewById(R.id.list_search);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        list_produk = new ArrayList<>();

        searchView = (MaterialSearch) findViewById(R.id.search_view);
        searchView.setHint("Cari Nama Produk");
        searchView.showSearch();

        searchView.showVoice(true);
        searchView.setVoiceSearch(true);

        searchView.setOnQueryTextListener(new MaterialSearch.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                cariData(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                cariData(newText);
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void cariData(String query)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        final Call<JSONResponse> produk = request.getCari(query);
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse result = response.body();

                list_produk = new ArrayList<>(Arrays.asList(result.getProduk()));
                adapter = new ProdukAdapter(SearchActivity.this, list_produk);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }
}
