package luwakdev.bestonestore.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.Arrays;

import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.Produk;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Adapter.ProdukAdapter;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProdukActivity extends AppCompatActivity {

    String id;
    Bundle bundle;

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private ArrayList<Produk> list_produk;
    private ProdukAdapter adapter;
    private ImageView produk_kosong;
    private ProgressBar load_produk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_produk);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Daftar Barang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        load_produk = (ProgressBar) findViewById(R.id.load_produk);
        produk_kosong = (ImageView) findViewById(R.id.produk_kosong);

        recyclerView = (RecyclerView) findViewById(R.id.list_produk);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        list_produk = new ArrayList<>();

        bundle = getIntent().getExtras();
        id = bundle.getString("id");

        int dimen = (int) getResources().getDimension(R.dimen.def_produk);

        Log.v("ProdukActivity", "dimen : "+dimen);
        load_produk.getLayoutParams().width = dimen;
        load_produk.getLayoutParams().height = dimen;

        produk_kosong.getLayoutParams().width = dimen;
        produk_kosong.getLayoutParams().height = dimen;

        loadJSON();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(dy > 0) //check for scroll down
                {
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            Log.d("LOG_COBA", "Last Item Wow !");
                            //Do pagination.. i.e. fetch new data
                        }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadJSON()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        final Call<JSONResponse> produk = request.getProduk(id);
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse result = response.body();

                if (result.getProduk().length > 0) {
                    produk_kosong.setVisibility(View.GONE);

                    list_produk = new ArrayList<>(Arrays.asList(result.getProduk()));
                    adapter = new ProdukAdapter(ProdukActivity.this, list_produk);

                    load_produk.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    getSupportActionBar().setTitle(result.nama_kategori());
                }
                else {
                    load_produk.setVisibility(View.GONE);
                    produk_kosong.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }
}
