package luwakdev.bestonestore.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.API.User;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Auth.SessionManager;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyEditText;
import ws.MyTextView;

public class ActivitySignin extends AppCompatActivity implements Validator.ValidationListener {

    MyTextView signin;
    private static final String TAG = "ActivitySignIn";

    @NotEmpty
    @Email
    MyEditText emails;

    @NotEmpty
    @Password
    MyEditText passw;

    Validator validator;
    SharedPreferences sp;
    SessionManager session;
    SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.signin);

        validator = new Validator(this);
        validator.setValidationListener(this);

        db = new SQLiteHandler(this);
        session = new SessionManager(this);

        /*if (session.isLoggedIn())
        {
            startActivity(new Intent(ActivitySignin.this, MainActivity.class));
        }*/

        signin = (MyTextView)findViewById(R.id.signin);
        emails = (MyEditText) findViewById(R.id.username);
        passw = (MyEditText) findViewById(R.id.password);

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        String email = emails.getText().toString();
        String password = passw.getText().toString();
        loginUser(email, password);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof MyEditText) {
                ((MyEditText) view).setError(message);
            } else {
                new SweetAlertDialog(ActivitySignin.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(message)
                        .setCancelText("Okay")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        }
    }

    public void loginUser(String email, String password)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        System.out.println("URL : "+retrofit.baseUrl());
        RestAPI request = retrofit.create(RestAPI.class);

        Call<User> user = request.loginUser(email, password);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                User user = response.body();
                System.out.println("Cek Respon : "+user.getName());
                System.out.println("Pesan : "+user.getPesan());
                System.out.println("Status : "+user.getStatus());
                int status = Integer.parseInt(user.getStatus());
                String pesan = user.getPesan();

                if (status == 0) {
                    new SweetAlertDialog(ActivitySignin.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText(pesan)
                            .show();
                } else {
                    final String id = user.getId();
                    final String name = user.getName();
                    final String email = user.getEmail();
                    final String alamat = user.getAlamat();
                    final String telp = user.getTelp();
                    final String role = user.getRole();
//                    final String password = user.getPassword();
                    final String provinsi = user.getProvinsi();
                    final String kota = user.getKota();
                    final String zipcode = user.getZipcode();
                    System.out.println("Hasil : "+name+"Email : "+email+"Alamat : "+alamat);

                    final SweetAlertDialog tamvanDialog = new SweetAlertDialog(ActivitySignin.this, SweetAlertDialog.SUCCESS_TYPE);
                    tamvanDialog.setTitleText("Berhasil");
                    tamvanDialog.setContentText(""+pesan);
                    tamvanDialog.setConfirmText("Ok");
                    tamvanDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            tamvanDialog.dismissWithAnimation();
                            db.addUser(id, name, email, alamat, telp, provinsi, kota, zipcode);
                            GoLang();
                        }
                    }, 1500);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG_ERROR",""+t.getMessage().toString());
                new SweetAlertDialog(ActivitySignin.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        new SweetAlertDialog(ActivitySignin.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Peringatan !")
                .setContentText("Apakah Anda Ingin Batal Login?")
                .setConfirmText("Ya")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        startActivity(new Intent(ActivitySignin.this, HomeActivity.class));
                        finish();
                    }
                })
                .setCancelText("Tidak")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    public void GoLang()
    {
        session.setLogin(true);
        startActivity(new Intent(ActivitySignin.this, MainActivity.class));
        finish();
    }
}