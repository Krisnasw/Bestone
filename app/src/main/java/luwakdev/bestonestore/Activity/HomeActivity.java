package luwakdev.bestonestore.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Auth.SessionManager;
import luwakdev.bestonestore.R;

public class HomeActivity extends AppCompatActivity {

    TextView signin;
    LinearLayout get;
    SessionManager session;
    SQLiteHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_home);

        get = (LinearLayout)findViewById(R.id.get);
        signin = (TextView)findViewById(R.id.signin);

        session = new SessionManager(this);
        db = new SQLiteHandler(this);

        if (session.isLoggedIn())
        {
            startActivity(new Intent(HomeActivity.this, MainActivity.class));
            finish();
        }

        get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(HomeActivity.this, ActivitySignup.class);
                startActivity(it);
                finish();

            }
        });

        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent it = new Intent(HomeActivity.this,ActivitySignin.class);
                startActivity(it);
                finish();
            }
        });

    }
}