package luwakdev.bestonestore.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import luwakdev.bestonestore.API.Artikel;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyTextView;

import static luwakdev.bestonestore.R.id.deskripsi_barang;
import static luwakdev.bestonestore.R.id.produk_nama;

/**
 * Created by Krisnasw on 10/26/2016.
 */

public class NewsActivity extends AppCompatActivity {
    String id;
    Bundle bundle;

    ImageView gambar_blog;
    LinearLayout linear1,linear2;
    TextView deskripsi;

    MyTextView nama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.detail_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Detail Artikel");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bundle = getIntent().getExtras();
        id = bundle.getString("id");

        getDetailArtikel(id);

        linear1 = (LinearLayout)findViewById(R.id.linear1);
        linear2 = (LinearLayout)findViewById(R.id.linear2);
        deskripsi = (TextView)findViewById(deskripsi_barang);

        gambar_blog = (ImageView) findViewById(R.id.slider);
        nama = (MyTextView) findViewById(produk_nama);

        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear2.setVisibility(View.VISIBLE);
                linear1.setVisibility(View.GONE);
                deskripsi.setVisibility(View.VISIBLE);
            }
        });

        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear2.setVisibility(View.GONE);
                linear1.setVisibility(View.VISIBLE);
                deskripsi.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getDetailArtikel(String id)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<Artikel> artikel = request.getDetailArtikel(id);
        artikel.enqueue(new Callback<Artikel>() {
            @Override
            public void onResponse(Call<Artikel> call, Response<Artikel> response) {

                Artikel artikel = response.body();
                System.out.println("Judul : "+artikel.getJudul());
                Glide.with(NewsActivity.this)
                        .load(AppConfig.IMAGE_URL + artikel.getImage())
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(gambar_blog);
                nama.setText(artikel.getJudul());
                deskripsi.setText(artikel.getDeskripsi());
            }

            @Override
            public void onFailure(Call<Artikel> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }
}