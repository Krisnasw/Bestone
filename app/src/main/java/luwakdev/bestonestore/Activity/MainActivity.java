package luwakdev.bestonestore.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Auth.SessionManager;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.Fragment.ArtikelFragment;
import luwakdev.bestonestore.Fragment.BerandaFragment;
import luwakdev.bestonestore.Fragment.ChatFragment;
import luwakdev.bestonestore.Other.ImageTransform;
import luwakdev.bestonestore.R;

public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView foto_user;
    private TextView nama_user, email_user;
    private Toolbar toolbar;

    SQLiteHandler db;
    SessionManager session;
    HashMap<String, String> user;
    String jeneng,emaile;

    private static final String urlProfileImg = AppConfig.ROOT_URL + "images/users/luwak.jpg";

    public static int navItemIndex = 0;

    private static final String TAG_BERANDA = "Beranda";
    private static final String TAG_ARTIKEL = "Artikel";
    private static final String TAG_CHAT = "Customer Service";
    private static final String TAG_SHARE = "Share";
    private static final String TAG_LOGOUT = "Logout";

    public static String CURRENT_TAG = TAG_BERANDA;

    private String[] activityTitles;

    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(this);

        if (session.isLoggedIn() == false)
        {
            startActivity(new Intent(MainActivity.this, HomeActivity.class));
        }

        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        navHeader = navigationView.getHeaderView(0);

        nama_user = (TextView) navHeader.findViewById(R.id.nama_user);
        email_user = (TextView) navHeader.findViewById(R.id.email_user);
        foto_user = (ImageView) navHeader.findViewById(R.id.foto_user);

        user = db.getUserDetails();
        jeneng = user.get("name");
        emaile = user.get("email");
        System.out.println("Biasane : "+jeneng);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        loadNavHeader();

        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 0;
            CURRENT_TAG = TAG_BERANDA;
            loadHomeFragment();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_BERANDA;
                loadHomeFragment();
                return;
            }
        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
            return true;
        }
        else if (id == R.id.action_cart) {
            startActivity(new Intent(MainActivity.this, CartActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void loadNavHeader() {
        nama_user.setText(jeneng);
        email_user.setText(emaile);

        Glide.with(this).load(urlProfileImg)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new ImageTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(foto_user);
    }

    private void loadHomeFragment() {
        selectNavMenu();

        setToolbarTitle();

        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            return;
        }

        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        drawer.closeDrawers();

        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                BerandaFragment berandaFragment = new BerandaFragment();
                return berandaFragment;
            case 1:
                ArtikelFragment artikelFragment = new ArtikelFragment();
                return artikelFragment;
            default:
                return new BerandaFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                switch (menuItem.getItemId()) {
                    case R.id.nav_beranda:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_BERANDA;
                        break;
                    case R.id.nav_artikel:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_ARTIKEL;
                        break;
                    case R.id.nav_chat:
//                        navItemIndex = 2;
                        CURRENT_TAG = TAG_CHAT;
                        Intent myIntent = new Intent(MainActivity.this, JivoActivity.class);
                        startActivity(myIntent);
                        break;
                    case R.id.nav_logout:
//                        navItemIndex = 2;
                        CURRENT_TAG = TAG_LOGOUT;
                        new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Notifikasi ?")
                                .setContentText("Apakah Anda Yakin Ingin Logout ?")
                                .setConfirmText("Ya")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        session.setLogin(false);
                                        startActivity(new Intent(MainActivity.this, HomeActivity.class));
                                        finish();
                                    }
                                })
                                .setCancelText("Tidak")
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismissWithAnimation();
                                    }
                                })
                                .show();
                        break;
                    default:
                        navItemIndex = 0;
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();
    }
}
