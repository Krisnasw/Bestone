package luwakdev.bestonestore.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.API.User;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Auth.SessionManager;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyEditText;
import ws.MyTextView;

public class ActivitySignup extends AppCompatActivity implements Validator.ValidationListener {

    Validator validator;
    MyTextView signup;
    SessionManager session;
    SQLiteHandler db;

    @NotEmpty
    MyEditText fullname;
    @NotEmpty
    @Email
    MyEditText emails;
    @NotEmpty
    @Password(min = 6)
    MyEditText passw;
    @NotEmpty
    MyEditText address;
    @NotEmpty
    MyEditText nohp;
    @NotEmpty
    MyEditText prov;
    @NotEmpty
    MyEditText city;
    @NotEmpty
    MyEditText kodepos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.signup);

        validator = new Validator(this);
        validator.setValidationListener(this);

        db = new SQLiteHandler(this);
        session = new SessionManager(this);

        /*if (session.isLoggedIn())
        {
            startActivity(new Intent(ActivitySignup.this, MainActivity.class));
        }*/

        signup = (MyTextView) findViewById(R.id.signup);
        fullname = (MyEditText) findViewById(R.id.fullname);
        passw = (MyEditText) findViewById(R.id.passw);
        emails = (MyEditText) findViewById(R.id.email);
        address = (MyEditText) findViewById(R.id.address);
        nohp = (MyEditText) findViewById(R.id.nohp);
        prov = (MyEditText) findViewById(R.id.prov);
        city = (MyEditText) findViewById(R.id.city);
        kodepos = (MyEditText) findViewById(R.id.kodepos);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate(true);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        String name = fullname.getText().toString();
        String email = emails.getText().toString();
        String password = passw.getText().toString();
        String alamat = address.getText().toString();
        String telp = nohp.getText().toString();
        String provinsi = prov.getText().toString();
        String kota = city.getText().toString();
        String zipcode = kodepos.getText().toString();
        registerUser(name, email, password, alamat, telp, provinsi, kota, zipcode);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof MyEditText) {
                ((MyEditText) view).setError(message);
            } else {
                new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(message)
                        .setCancelText("Okay")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        }
    }

    public void registerUser(String name, String email, String password, String alamat, String telp, String provinsi, String kota, String zipcode)
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        System.out.println("URL : "+retrofit.baseUrl());
        RestAPI request = retrofit.create(RestAPI.class);

        Call<User> user = request.registerUser(name,email,password,alamat,telp,provinsi,kota,zipcode);
        user.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.isSuccessful()) {
                    User user = response.body();
                    System.out.println("Cek Respon : "+response.body());
                    int status = Integer.parseInt(user.getStatus());
                    String pesan = user.getPesan();

                    if (status == 0) {
                        new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!")
                                .setContentText(pesan)
                                .show();
                    } else {
                        new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Pendaftaran Berhasil")
                                .setContentText(pesan)
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        startActivity(new Intent(ActivitySignup.this, ActivitySignin.class));
                                        finish();
                                    }
                                })
                                .show();
                    }
                } else {
                    System.out.println("Errornya :" +response.errorBody().toString());
                    new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!")
                            .setContentText(response.errorBody().toString())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("TAG_ERROR",""+t.getMessage().toString());
                new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        new SweetAlertDialog(ActivitySignup.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Peringatan !")
                .setContentText("Apakah Anda Ingin Batal Mendaftar?")
                .setConfirmText("Ya")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        startActivity(new Intent(ActivitySignup.this, HomeActivity.class));
                        finish();
                    }
                })
                .setCancelText("Tidak")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }
}