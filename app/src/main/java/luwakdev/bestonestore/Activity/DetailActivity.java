package luwakdev.bestonestore.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;

import java.text.DecimalFormat;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.Produk;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyTextView;
import ws.SliderLayout;

public class DetailActivity extends AppCompatActivity {

    String id, id_user;
    Integer harga_barang;
    Bundle bundle;

    SQLiteHandler db;
    HashMap<String, String> user;

    SliderLayout mDemoSlider;
    LinearLayout linear1,linear2, linear3, linear4;
    TextView deskripsi, kat_barang;

    MyTextView status, nama, harga, beli;

    HashMap<Integer, String> file_maps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Detail Barang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bundle = getIntent().getExtras();
        id = bundle.getString("id");

        System.out.println("Id Produk : " + id);

        db = new SQLiteHandler(this);
        user = db.getUserDetails();
        id_user = user.get("id_user");

        getDetail();

        init();

//         ********Slider*********

        mDemoSlider = (SliderLayout)findViewById(R.id.slider);

        file_maps = new HashMap<Integer, String>();
    }

    private void init()
    {
        linear1 = (LinearLayout) findViewById(R.id.linear1);
        linear2 = (LinearLayout) findViewById(R.id.linear2);

        deskripsi = (TextView) findViewById(R.id.deskripsi_barang);
        kat_barang = (TextView) findViewById(R.id.kat_barang);

        status = (MyTextView) findViewById(R.id.produk_status);
        nama = (MyTextView) findViewById(R.id.produk_nama);
        harga = (MyTextView) findViewById(R.id.produk_harga);
        beli = (MyTextView) findViewById(R.id.beli);

        linear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear2.setVisibility(View.VISIBLE);
                linear1.setVisibility(View.GONE);
                deskripsi.setVisibility(View.VISIBLE);

            }
        });

        linear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear2.setVisibility(View.GONE);
                linear1.setVisibility(View.VISIBLE);
                deskripsi.setVisibility(View.GONE);
            }
        });

//                ***********use and care**********

        linear3 = (LinearLayout)findViewById(R.id.linear3);
        linear4 = (LinearLayout)findViewById(R.id.linear4);

        linear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear4.setVisibility(View.VISIBLE);
                linear3.setVisibility(View.GONE);
                kat_barang.setVisibility(View.VISIBLE);
            }
        });

        linear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linear4.setVisibility(View.GONE);
                linear3.setVisibility(View.VISIBLE);
                kat_barang.setVisibility(View.GONE);
            }
        });

        beli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addBeli();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    public void getDetail()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.getDetail(id);
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();
                Produk hasil = json.getDetail();

                DecimalFormat format = new DecimalFormat("#,###");
                harga_barang = Integer.parseInt(hasil.getHarga());

                status.setText(hasil.getStatus().toUpperCase());
                nama.setText(hasil.getName_produk());
                harga.setText("Rp. " + format.format(harga_barang) + " ,-");

                deskripsi.setText(Html.fromHtml(hasil.getDeskripsi()));
                kat_barang.setText(hasil.getNama_kategori());

                file_maps.put(1, AppConfig.IMAGE_URL + hasil.getImage());
                mDemoSlider.stopAutoCycle();
                mDemoSlider.setPagerTransformer(false, new BaseTransformer() {
                    @Override
                    protected void onTransform(View view, float v) {
                    }
                });

                for(Integer name : file_maps.keySet()){
                    TextSliderView textSliderView = new TextSliderView(getApplication());

                    textSliderView
                            .image(file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle().putInt("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }

    public void addBeli()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.addBeli(
                id_user,
                Integer.parseInt(id),
                1,
                harga_barang,
                harga_barang
        );
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();

                if (json.getStatus() == 1) {
                    final SweetAlertDialog dialog = new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setTitleText("Berhasil");
                    dialog.setContentText(json.getPesan());
                    dialog.setConfirmText("Ok");
                    dialog.show();

                    SharedPreferences sp = getSharedPreferences("produk", MODE_PRIVATE);
                    SharedPreferences.Editor ed = sp.edit();

                    ed.putInt("id_produk", Integer.parseInt(id));
                    ed.commit();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismissWithAnimation();
                            onBackPressed();
                        }
                    }, 1000);
                }
                else {
                    new SweetAlertDialog(DetailActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Gagal !")
                            .setContentText(json.getPesan())
                            .show();
                }

            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }
}
