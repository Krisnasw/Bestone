package luwakdev.bestonestore.API;

/**
 * Created by decrypt21 on 11/6/16.
 */

public class Cart {

    private String id_produk;
    private String name_produk;
    private String harga_asli;
    private String sub;
    private String image;
    private String qty_barang;

    public String getId_produk() {
        return id_produk;
    }

    public String getName_produk() {
        return name_produk;
    }

    public String getHarga_asli() {
        return harga_asli;
    }

    public String getSub() {
        return sub;
    }

    public String getImage() {
        return image;
    }

    public String getQty_barang() {
        return qty_barang;
    }
}
