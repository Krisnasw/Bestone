package luwakdev.bestonestore.API;

/**
 * Created by Krisnasw on 10/23/2016.
 */

public class User {

    private String id;
    private String name;
    private String email;
//    private String password;
    private String alamat;
    private String telp;
    private String role;
    private String provinsi;
    private String kota;
    private String zipcode;
    private String status;
    private String pesan;

    public User(String id, String name, String email, String alamat, String telp, String role,
                String provinsi, String kota, String zipcode, String status, String pesan) {
        this.id = id;
        this.name = name;
        this.email = email;
//        this.password = password;
        this.alamat = alamat;
        this.telp = telp;
        this.role = role;
        this.provinsi = provinsi;
        this.kota = kota;
        this.zipcode = zipcode;
        this.status = status;
        this.pesan = pesan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}