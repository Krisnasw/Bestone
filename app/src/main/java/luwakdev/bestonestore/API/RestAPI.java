package luwakdev.bestonestore.API;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by decrypt21 on 10/16/16.
 */

public interface RestAPI {

    @POST("artikel.php")
    Call<JSONResponse> getArtikel();


    @GET("detail_artikel.php")
    Call<Artikel> getDetailArtikel(
            @Query("id") String id
    );

    @POST("list_kategori.php")
    Call<JSONResponse> getListKat();

    @GET("produk.php")
    Call<JSONResponse> getProduk(
            @Query("param") String param
    );

    @GET("detail_produk.php")
    Call<JSONResponse> getDetail(
            @Query("id") String id
    );

    @GET("cari_produk.php")
    Call<JSONResponse> getCari(
            @Query("query") String query
    );

    @POST("user/login")
    Call<User> loginUser(
            @Query("email") String email,
            @Query("password") String password
    );

    @POST("user/register")
    Call<User> registerUser(
            @Query("name") String name,
            @Query("email") String email,
            @Query("password") String password,
            @Query("alamat") String alamat,
            @Query("telp") String telp,
            @Query("provinsi") String provinsi,
            @Query("kota") String kota,
            @Query("zipcode") String zipcode
    );

    @POST("temps")
    Call<JSONResponse> addBeli(
            @Query("id_user") String id_user,
            @Query("id_produk") int id_produk,
            @Query("qty_barang") int qty_barang,
            @Query("harga_asli") int harga_asli,
            @Query("sub") int sub
    );

    @GET("datacart/{id_user}")
    Call<JSONResponse> getBeli(
            @Path("id_user") String id_user
    );

    @POST("deleteCart")
    Call<JSONResponse> hapusItem(
            @Query("id_user") String id_user,
            @Query("id_produk") String id_produk
    );

    @POST("ngorder")
    Call<JSONResponse> tambahOrder(
            @Query("id_user") String id_user,
            @Query("id_produk") int id_produk,
            @Query("quantity") int quantity,
            @Query("nama_pemesan") String nama_pemesan,
            @Query("alamat") String alamat,
            @Query("telp") String telp,
            @Query("jumlah_dibayar") int jumlah_dibayar,
            @Query("method") String method,
            @Query("keterangan") String keterangan
    );

    @POST("updateTemps")
    Call<JSONResponse> updateQty(
            @Query("id_produk") String id_produk,
            @Query("id_user") String id_user,
            @Query("qty") Integer qty,
            @Query("harga_asli") Integer harga_asli
    );

    @POST("deleteorderan/{id_user}")
    Call<JSONResponse> hapusCart(
            @Path("id_user") String id_user
    );
}