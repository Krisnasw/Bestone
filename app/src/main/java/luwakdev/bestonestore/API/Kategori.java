package luwakdev.bestonestore.API;

/**
 * Created by decrypt21 on 10/28/16.
 */

public class Kategori {

    private String id;
    private String nama_kategori;
    private String image;

    public Kategori(String id, String nama_kategori, String image) {
        this.id = id;
        this.nama_kategori = nama_kategori;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
