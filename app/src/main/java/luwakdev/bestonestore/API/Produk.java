package luwakdev.bestonestore.API;

/**
 * Created by decrypt21 on 10/16/16.
 */

public class Produk {

    private String id;
    private String image;
    private String status;
    private String name_produk;
    private String harga;
    private String deskripsi;
    private String nama_kategori;
    private String slug;

    public Produk(String id, String image, String status, String name_produk, String harga, String deskripsi, String nama_kategori, String slug) {
        this.id = id;
        this.image = image;
        this.status = status;
        this.name_produk = name_produk;
        this.harga = harga;
        this.deskripsi = deskripsi;
        this.nama_kategori = nama_kategori;
        this.slug = slug;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName_produk() {
        return name_produk;
    }

    public void setName_produk(String name_produk) {
        this.name_produk = name_produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
