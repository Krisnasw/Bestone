package luwakdev.bestonestore.API;

/**
 * Created by decrypt21 on 10/16/16.
 */

public class JSONResponse {

    private Produk[] produk;
    private Slider[] slider;
    private Artikel[] artikel;

    private Produk detail;
    private Slider[] detail_slider;

    private Kategori[] list_kat;

    private Cart[] daftar_beli;
    private int total;

    private int status;
    private String pesan;

    public Produk[] getProduk() {
        return produk;
    }

    public Slider[] getSlider() {
        return slider;
    }

    public String gambar_slider(int param)
    {
        return slider[param].getImage().toString();
    }

    public Produk getDetail() {
        return detail;
    }

    public Slider[] getDetail_slider() {
        return detail_slider;
    }

    public Kategori[] getList_kat() {
        return list_kat;
    }

    public String gambar_detail(int param)
    {
        return detail_slider[param].getImage().toString();
    }

    public String nama_kategori(){
        return list_kat[0].getNama_kategori().toString();
    }

    public Artikel[] getArtikel() {
        return artikel;
    }

    public Cart[] getDaftar_beli() {
        return daftar_beli;
    }

    public int getTotal() {
        return total;
    }

    public int getStatus() {
        return status;
    }

    public String getPesan() {
        return pesan;
    }
}