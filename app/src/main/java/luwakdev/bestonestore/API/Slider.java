package luwakdev.bestonestore.API;

/**
 * Created by decrypt21 on 10/18/16.
 */

public class Slider {

    private String image;

    public Slider(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
