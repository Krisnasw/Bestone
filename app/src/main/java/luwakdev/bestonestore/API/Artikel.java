package luwakdev.bestonestore.API;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Krisnasw on 10/23/2016.
 */

public class Artikel {

    @SerializedName("id")
    public String id;
    @SerializedName("judul")
    public String judul;
    @SerializedName("slug")
    public String slug;
    @SerializedName("author")
    public String author;
    @SerializedName("dibaca")
    public String dibaca;
    @SerializedName("image")
    public String image;
    @SerializedName("deskripsi")
    public String deskripsi;
    @SerializedName("created_at")
    public String created_at;
    @SerializedName("updated_at")
    public String updated_at;

    public Artikel(String id, String judul, String slug, String author, String dibaca,
                   String image, String deskripsi, String created_at, String updated_at) {
        this.id = id;
        this.judul = judul;
        this.slug = slug;
        this.author = author;
        this.dibaca = dibaca;
        this.image = image;
        this.deskripsi = deskripsi;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDibaca() {
        return dibaca;
    }

    public void setDibaca(String dibaca) {
        this.dibaca = dibaca;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
