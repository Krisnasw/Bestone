package luwakdev.bestonestore.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.Artikel;
import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.ExpandableHeightListView;
import ws.ListBaseAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtikelFragment extends Fragment {

    public ArtikelFragment() {
        // Required empty public constructor
    }

    private ExpandableHeightListView listview;
    private ArrayList<Artikel> Bean;
    private ListBaseAdapter baseAdapter;

    ProgressBar progress_dialog;
    ScrollView scroll_view;

    View view;

    HashMap<Integer,String> file_maps;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_artikel, container, false);
        // Inflate the layout for this fragment

        progress_dialog = (ProgressBar) view.findViewById(R.id.progress_dialog2);
        scroll_view = (ScrollView) view.findViewById(R.id.scroll_view2);

        listview = (ExpandableHeightListView)view.findViewById(R.id.listview);

        Bean = new ArrayList<>();

        loadJSON();

        file_maps = new HashMap<Integer, String>();

        return view;
    }

    public void loadJSON()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> artikel = request.getArtikel();
        artikel.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();
                System.out.println("Hasil : "+jsonResponse);

                /* Menampilkan List Produk */
                Bean = new ArrayList<>(Arrays.asList(jsonResponse.getArtikel()));

                baseAdapter = new ListBaseAdapter(getActivity(), Bean);
                listview.setExpanded(true);

                progress_dialog.setVisibility(View.GONE);
                scroll_view.setVisibility(View.VISIBLE);

                listview.setAdapter(baseAdapter);
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!")
                        .setContentText(t.getMessage())
                        .setCancelText("Dismiss")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
            }
        });
    }

}
