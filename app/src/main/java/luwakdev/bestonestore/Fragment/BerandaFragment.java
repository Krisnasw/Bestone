package luwakdev.bestonestore.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.Kategori;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.API.Slider;
import luwakdev.bestonestore.Activity.JivoActivity;
import luwakdev.bestonestore.Activity.MainActivity;
import luwakdev.bestonestore.Adapter.KategoriAdapter;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.ExpandableHeightGridView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BerandaFragment extends Fragment {

    private ExpandableHeightGridView gridview;
    private KategoriAdapter kategoriAdapter;

    private ArrayList<Kategori> list_kategori;
    private ArrayList<Slider> slider_list;

    ProgressBar progress_dialog;
    ScrollView scroll_view;
    SliderLayout mDemoSlider;

    HashMap<Integer,String> file_maps;

    public BerandaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_beranda, container, false);
        // Inflate the layout for this fragment

        progress_dialog = (ProgressBar) view.findViewById(R.id.progress_dialog);
        scroll_view = (ScrollView) view.findViewById(R.id.scroll_view);

        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);
        gridview = (ExpandableHeightGridView) view.findViewById(R.id.gridview);

        list_kategori = new ArrayList<>();
        slider_list = new ArrayList<>();

        loadJSON();

        file_maps = new HashMap<Integer, String>();

        return view;
    }

    public void loadJSON()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.getListKat();
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse jsonResponse = response.body();

                /* Menampilkan List Produk */
                list_kategori = new ArrayList<>(Arrays.asList(jsonResponse.getList_kat()));

                kategoriAdapter = new KategoriAdapter(getActivity(), list_kategori);
                gridview.setExpanded(true);

                progress_dialog.setVisibility(View.GONE);
                scroll_view.setVisibility(View.VISIBLE);

                gridview.setAdapter(kategoriAdapter);

                /* Menampilkan Slider Dinamis */
                for (int i=1; i<=jsonResponse.getSlider().length;i++)
                {
                    file_maps.put(i, AppConfig.IMAGE_URL + jsonResponse.gambar_slider(i-1));
                }

                for(Integer name : file_maps.keySet()){
                    TextSliderView textSliderView = new TextSliderView(getActivity());

                    textSliderView
                            .image(file_maps.get(name))
                            .setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle().putInt("extra", name);

                    mDemoSlider.addSlider(textSliderView);
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }
}
