package luwakdev.bestonestore.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.daimajia.slider.library.SliderLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import luwakdev.bestonestore.API.Kategori;
import luwakdev.bestonestore.API.Slider;
import luwakdev.bestonestore.Activity.JivoActivity;
import luwakdev.bestonestore.Activity.MainActivity;
import luwakdev.bestonestore.Adapter.KategoriAdapter;
import luwakdev.bestonestore.R;
import luwakdev.bestonestore.Widget.JivoDelegate;
import luwakdev.bestonestore.Widget.JivoSdk;
import ws.ExpandableHeightGridView;

/**
 * Created by krisnasw on 02/12/16.
 */

public class ChatFragment extends Fragment implements JivoDelegate {

    JivoSdk jivoSdk;

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_jivo, container, false);
        // Inflate the layout for this fragment

        String lang = Locale.getDefault().getLanguage().indexOf("en") >= 0 ? "en": "en";

        jivoSdk = new JivoSdk((WebView) view.findViewById(R.id.webview), lang);
        jivoSdk.delegate = this;
        jivoSdk.prepare();

        return view;
    }

    @Override
    public void onEvent(String name, String data) {
        if(name.equals("url.click")){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data));
            startActivity(browserIntent);
        }
    }
}