package luwakdev.bestonestore.Cart;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Activity.CartActivity;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyEditText;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemCart2 extends Fragment implements Validator.ValidationListener {

    View view;
    String id_user;

    @NotEmpty
    MyEditText nama_pemesan, alamat, telp, keterangan;
    Spinner tipe_bayar;
    TextView submit_order;

    SQLiteHandler db;
    HashMap<String, String> user;
    int id_produk, totalnya;
    Validator validator;

    public ItemCart2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.item_cart_2, container, false);

        db = new SQLiteHandler(getContext());
        validator = new Validator(this);
        validator.setValidationListener(this);
        user = db.getUserDetails();

        id_user = user.get("id_user");
        SharedPreferences sp = this.getActivity().getSharedPreferences("produk", Context.MODE_PRIVATE);
        id_produk = sp.getInt("id_produk", 0);

        SharedPreferences gg = this.getActivity().getSharedPreferences("tamvan", Context.MODE_PRIVATE);
        totalnya = gg.getInt("total", 0);

        nama_pemesan = (MyEditText) view.findViewById(R.id.nama_pembeli);
        alamat = (MyEditText) view.findViewById(R.id.alamat);
        telp = (MyEditText) view.findViewById(R.id.no_telp);
        tipe_bayar = (Spinner) view.findViewById(R.id.tipe_bayar);
        keterangan = (MyEditText) view.findViewById(R.id.keterangan);

        submit_order = (TextView) view.findViewById(R.id.submit_order);
        submit_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validator.validate();
            }
        });

        return view;
    }

    public void inputOrder()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        String nama = nama_pemesan.getText().toString();
        String alamatx = alamat.getText().toString();
        String telpx = telp.getText().toString();
        String tipe = tipe_bayar.getSelectedItem().toString();
        String isi = keterangan.getText().toString();

        Call<JSONResponse> produk = request.tambahOrder(
                id_user,
                id_produk,
                1,
                nama,
                alamatx,
                telpx,
                totalnya,
                tipe,
                isi
        );

        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();

                if (json.getStatus() == 1) {
                    final SweetAlertDialog dialog = new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE);
                    dialog.setTitleText("Berhasil");
                    dialog.setContentText(json.getPesan());
                    dialog.setConfirmText("Ok");
                    dialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismissWithAnimation();
                            ((CartActivity) getActivity()).finish();
                        }
                    }, 1000);
                }
                else {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Gagal !")
                            .setContentText(json.getPesan())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        inputOrder();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Error!")
                .setContentText("Harap Lengkapi Data!")
                .show();
    }
}