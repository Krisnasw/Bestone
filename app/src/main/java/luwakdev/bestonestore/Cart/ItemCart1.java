package luwakdev.bestonestore.Cart;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import luwakdev.bestonestore.API.Cart;
import luwakdev.bestonestore.API.JSONResponse;
import luwakdev.bestonestore.API.RestAPI;
import luwakdev.bestonestore.Activity.CartActivity;
import luwakdev.bestonestore.Adapter.BeliAdapter;
import luwakdev.bestonestore.Auth.SQLiteHandler;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ws.MyTextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ItemCart1 extends Fragment {

    View view;
    MyTextView lanjut, harga_total;
    TextView tgl_beli, tgl_beli2;

    String id_user;
    String mHari;
    int mTanggal;
    int mBulan;
    int mTahun;

    SQLiteHandler db;
    HashMap<String, String> user;

    ProgressBar progress_dialog;
    ListView listview;
    private ArrayList<Cart> list_cart;
    private BeliAdapter beliAdapter;

    public ItemCart1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.item_cart_1, container, false);

        tgl_beli = (TextView) view.findViewById(R.id.tgl_beli);
        tgl_beli2 = (TextView) view.findViewById(R.id.tgl_beli2);

        lanjut = (MyTextView) view.findViewById(R.id.lanjut);
        harga_total = (MyTextView) view.findViewById(R.id.harga_total);

        lanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CartActivity) getActivity()).switchTab();
            }
        });

        db = new SQLiteHandler(getContext());
        user = db.getUserDetails();
        id_user = user.get("id_user");

        progress_dialog = (ProgressBar) view.findViewById(R.id.progress_dialog);
        listview = (ListView) view.findViewById(R.id.listview);
        list_cart = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        String[] days = new String[] { "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu" };

        mHari =  days[calendar.get(Calendar.DAY_OF_WEEK) - 1];
        mTahun = calendar.get(Calendar.YEAR);
        mBulan = calendar.get(Calendar.MONTH);
        mTanggal = calendar.get(Calendar.DATE);

        tgl_beli.setText(String.valueOf(mHari));
        tgl_beli2.setText(String.valueOf(
                convert_date(mTahun, mBulan, mTanggal)
        ));

        loadJSON();

        return view;
    }

    public void loadJSON()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.ROOT_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RestAPI request = retrofit.create(RestAPI.class);

        Call<JSONResponse> produk = request.getBeli(
                id_user
        );
        produk.enqueue(new Callback<JSONResponse>() {
            @Override
            public void onResponse(Call<JSONResponse> call, Response<JSONResponse> response) {

                JSONResponse json = response.body();

                /* Menampilkan List Produk */
                list_cart = new ArrayList<>(Arrays.asList(json.getDaftar_beli()));

                beliAdapter = new BeliAdapter(getActivity(), list_cart);

                progress_dialog.setVisibility(View.GONE);
                listview.setVisibility(View.VISIBLE);

                listview.setAdapter(beliAdapter);

                DecimalFormat format = new DecimalFormat("#,###");
                String total = format.format(json.getTotal());

                harga_total.setText("Rp. " + total + " ,-");

                SharedPreferences ps = getActivity().getSharedPreferences("tamvan", Context.MODE_PRIVATE);
                SharedPreferences.Editor ep = ps.edit();

                ep.putInt("total", json.getTotal());
                ep.commit();
            }

            @Override
            public void onFailure(Call<JSONResponse> call, Throwable t) {
                Log.d("TAG_ERROR",t.getMessage());
            }
        });
    }

    private String convert_date(int a, int b, int c) {
        String bulan = "";

        switch (b) {
            case 0: bulan = "Januari"; break;
            case 1: bulan = "Februari"; break;
            case 2: bulan = "Maret"; break;
            case 3: bulan = "April"; break;
            case 4: bulan = "Mei"; break;
            case 5: bulan = "Juni"; break;
            case 6: bulan = "Juli"; break;
            case 7: bulan = "Agustus"; break;
            case 8: bulan = "September"; break;
            case 9: bulan = "Oktober"; break;
            case 10: bulan = "November"; break;
            case 11: bulan = "Desember"; break;
        }

        return c + " " + bulan + " " + a;
    }
}