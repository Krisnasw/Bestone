package ws;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DecimalFormat;
import java.util.ArrayList;

import luwakdev.bestonestore.API.Produk;
import luwakdev.bestonestore.Activity.DetailActivity;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;


public class GridviewAdapter extends BaseAdapter {

    Context context;

    ArrayList<Produk> produk;
    Typeface fonts1, fonts2;

    Integer harga_barang;

    public GridviewAdapter(Context context, ArrayList<Produk> produk) {
        this.produk = produk;
        this.context = context;
    }

    @Override
    public int getCount() {
        return produk.size();
    }

    @Override
    public Object getItem(int position) {
        return produk.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        fonts1 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Light.ttf");

        fonts2 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");

        ViewHolder viewHolder = null;

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_beranda, null);

            viewHolder = new ViewHolder();

            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
//            viewHolder.status = (TextView) convertView.findViewById(R.id.status);
//            viewHolder.name_produk = (TextView) convertView.findViewById(R.id.name_produk);
//            viewHolder.harga = (TextView) convertView.findViewById(R.id.harga);

            viewHolder.status.setTypeface(fonts2);
            viewHolder.name_produk.setTypeface(fonts1);
            viewHolder.harga.setTypeface(fonts2);

            convertView.setTag(viewHolder);

        } else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Produk produk = (Produk) getItem(position);

        Glide.with(context).load(AppConfig.IMAGE_URL + produk.getImage())
                .placeholder(R.drawable.def_produk)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.image);

        DecimalFormat format = new DecimalFormat("#,###");
        harga_barang = Integer.parseInt(produk.getHarga());

        viewHolder.status.setText(produk.getStatus().toUpperCase());
        viewHolder.name_produk.setText(produk.getName_produk());
        viewHolder.harga.setText("Rp. " + format.format(harga_barang) + " ,-");

        final View finalConvertView = convertView;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(finalConvertView.getContext(), DetailActivity.class);
                intent.putExtra("id", produk.getId());
                finalConvertView.getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView image;
        TextView status;
        TextView name_produk;
        TextView harga;
    }
}