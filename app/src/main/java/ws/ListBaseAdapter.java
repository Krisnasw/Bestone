package ws;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import luwakdev.bestonestore.API.Artikel;
import luwakdev.bestonestore.Activity.NewsActivity;
import luwakdev.bestonestore.Config.AppConfig;
import luwakdev.bestonestore.R;

/**
 * Created by Krisnasw
 */

public class ListBaseAdapter extends BaseAdapter {

    Context context;

    ArrayList<Artikel> artikel;
    Typeface fonts1,fonts2;

    public ListBaseAdapter(Context context, ArrayList<Artikel> artikel) {

        this.context = context;
        this.artikel = artikel;
    }

    @Override
    public int getCount() {
        return artikel.size();
    }

    @Override
    public Object getItem(int position) {
        return artikel.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        fonts1 =  Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Light.ttf");

        fonts2 = Typeface.createFromAsset(context.getAssets(),
                "fonts/Lato-Regular.ttf");

        ViewHolder viewHolder = null;

        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_feeds,null);

            viewHolder = new ViewHolder();

            viewHolder.newsimage1 = (ImageView)convertView.findViewById(R.id.newsimage1);
            viewHolder.newsimage2 = (ImageView)convertView.findViewById(R.id.newsimage2);
            viewHolder.more = (ImageView)convertView.findViewById(R.id.more);
            viewHolder.newsname = (TextView)convertView.findViewById(R.id.newsname);
            viewHolder.time = (TextView)convertView.findViewById(R.id.time);
            viewHolder.news = (TextView)convertView.findViewById(R.id.news);

            viewHolder.newsname.setTypeface(fonts1);
            viewHolder.time.setTypeface(fonts1);
            viewHolder.news.setTypeface(fonts2);

            convertView.setTag(viewHolder);


        }else {

            viewHolder = (ViewHolder) convertView.getTag();
        }

        final Artikel bean = (Artikel) getItem(position);

        viewHolder.newsimage1.setImageResource(R.mipmap.ic_launcher);
        Glide.with(context).load(AppConfig.IMAGE_URL + bean.getImage())
                .placeholder(R.drawable.def_produk)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.newsimage2);
        viewHolder.more.setImageResource(R.drawable.more);
        viewHolder.newsname.setText(bean.getAuthor());
        viewHolder.time.setText(bean.getCreated_at());
        viewHolder.news.setText(bean.getJudul());

        final View finalConvertView = convertView;

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(finalConvertView.getContext(), NewsActivity.class);
                intent.putExtra("id", bean.getId());
                finalConvertView.getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    private class ViewHolder{

        ImageView newsimage1;
        ImageView newsimage2;
        ImageView more;
        TextView newsname;
        TextView time;
        TextView news;
    }
}